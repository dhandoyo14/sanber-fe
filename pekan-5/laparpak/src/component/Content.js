import React from "react";
import axios from "axios";
import { useEffect, useState } from "react";

const Content = ({products}) =>{
       
    return(
        <div className="content">
            <div className="bg-white">
                <div className="mx-auto max-w-2xl px-4 py-10 sm:px-6 sm:py-14 lg:max-w-7xl lg:px-8">
                <h2 className="font-semibold text-2xl font-semibold tracking-tight text-cyan-400 mb-6"> Catalog Product</h2>
            
                <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                    {products.map((item)=>(
                        <div className="">
                            
                            <div className="aspect-h-1 aspect-w-1 w-full h-2/3 overflow-hidden rounded-lg bg-gray-200 xl:aspect-h-8 xl:aspect-w-7">
                                <img src={item.image_url} alt="Sip" className="h-full w-full object-cover object-center group-hover:opacity-75"/>
                            </div>
                            <h3 className="mt-4 text-xl font-bold text-gray-700">{item.name}</h3>
                            {item.is_diskon&&<p className="mt-1 text-sm font-light text-lg font-bold text-red-400 line-through">{item.harga_diskon_display}</p>}
                            <p classNama="mt-1 text-lg font-medium text-gray-900">{item.harga_display}</p>
                            <p className="mt-1 text-xs font-medium text-blue-500">Stock {item.stock}</p>
                        </div>
                    ))}            
            
                </div>
                </div>
            </div>
          
        </div>
    )
}

export default Content;