import React from 'react';
import Form from './component/Form'
import Content from './component/Content'
import { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState("success");

  const fetchProducts = async () => {
    try {
        setLoading(true); 
        const response = await axios.get(
            "https://api-project.amandemy.co.id/api/products"
        );
        console.log(response.data.data);
        setProducts(response.data.data);
        setStatus("success");
        } catch (error) {
        console.log(error);
        setStatus("error");
        } finally {
        setLoading(false);
        }
    };

      useEffect(() => {
        console.log("Fetch Data");
        fetchProducts();
      }, []);
  return (
    <>
      <header className="mb-10">
      <nav className="w-full flex items-center justify-between flex-wrap bg-teal-500 p-6">
            <div className="flex items-center flex-shrink-0 text-white mr-6">
                <span className="font-semibold text-3xl tracking-tight">LaparPak</span>
            </div>
          </nav>
      </header>
      <main>
        <Form fetchProducts={fetchProducts}></Form>
        <Content products={products}></Content>
      </main>
    </>
  );
}

export default App;
