const sum = (...numbers) => numbers.reduce((result, item) => result + item);

console.log(sum(1,2,3,4,5,6))