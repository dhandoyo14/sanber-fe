const arr = [5, 3, 4, 7, 6, 9, 2];

const arrMap = arr.map(item => item % 2 == 0 ? item*2 : item*3);

console.log(arrMap);