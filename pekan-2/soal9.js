const luasPersegiPanjang = (panjang, lebar) => panjang * lebar;
const luas1 = luasPersegiPanjang(10, 5);
const luas2 = luasPersegiPanjang(2, 4);

console.log(luas1);
console.log(luas2);