import React  from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

const UpdateFrom = ({fetchProducts}) => {
  const { id } = useParams();

  const navigate = useNavigate();
  const [isDiskon, setDiskon] = useState(false);
  const [input, setInput] = useState({
      name: "",
      harga: 0,
      stock: 0,
      image_url: "",
      is_diskon: false,
      harga_diskon: 0,
      category: "makanan",
      description: "",
    });
  const handleChange = (event) => {
    console.log(event.target.value)
    if (event.target.name === "name") {
      setInput({ ...input, name: event.target.value });
    } else if (event.target.name === "harga") {
      setInput({ ...input, harga: event.target.value });
    } else if (event.target.name === "stock") {
      setInput({ ...input, stock: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...input, image_url: event.target.value }); // checkbox
    } else if (event.target.name === "is_diskon") {
      setInput({ ...input, is_diskon: event.target.checked }); // checkbox
    } else if (event.target.name === "harga_diskon") {
      setInput({ ...input, harga_diskon: event.target.value }); // checkbox
    } else if (event.target.name === "category") {
      setInput({ ...input, category: event.target.value }); // checkbox
    } else if (event.target.name === "description") {
      setInput({ ...input, description: event.target.value }); // checkbox
    }
    
  };

  const handleSubmit = async () => {
    try {
        console.log(input)
      const response = await axios.post(
        "https://api-project.amandemy.co.id/api/products",
        input
      );
      alert("Berhasil Mengirim Request");
      // memannggil data kembali
      // navigasi ke halaman table
      navigate("/table");
    } catch (error) {
      alert(error.response.data.info);
      console.log(error);
    }
  };

      
  useEffect(() => {
    console.log(`Fetch Product id ${id}`);
    fetchProductById();
  }, []);
    

  const fetchProductById = async () => {
    try {
      // fetch data
      const response = await axios.get(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      console.log(response.data.data);
      const product = response.data.data;
      // melakukan binding data dari server
      setInput({
        name: product.name,
        harga: product.harga,
        stock: product.stock,
        image_url: product.image_url,
        is_diskon: product.is_diskon,
        harga_diskon: product.harga_diskon,
        category: product.category,
        description: product.description
      });
    } catch (error) {
      console.log(error);
    }
  };
    
    return(     
        <div className="mt-0 content items-center flex justify-center">
            <div className="block max-w-sm p-6 bg-white border border-gray-200 rounded-lg content-center form-card">
                <form className="w-full max-w-sm lg:max-w-full" >
                    <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Nama Barang
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-product-name" checked={input.highlight} onChange={handleChange} 
          value={input.name}
 name="name" type="text" placeholder="Nama Barang"/>
                        <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Stok Barang
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-product-stock" checked={input.highlight} onChange={handleChange} 
          value={input.stock}
 name="stock" type="text" placeholder="0"/>
                    </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-2">
                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                        Harga
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-price" checked={input.highlight} onChange={handleChange} 
          value={input.harga}
 name="harga" type="text" placeholder="0"/>
                    </div>
                    <div className="w-full md:w-1/3 px-1 mb-6 md:mb-0 flex flex-row">
                        <input className="before:content[''] peer relative h-5 w-5 cursor-pointer appearance-none rounded-md border border-blue-gray-200 transition-all before:absolute before:top-2/4 before:left-2/4 before:block before:h-12 before:w-12 before:-translate-y-2/4 before:-translate-x-2/4 before:rounded-full before:bg-blue-gray-500 before:opacity-0 before:transition-opacity checked:border-teal-500 checked:bg-teal-500 checked:before:bg-teal-500 hover:before:opacity-10 mt-7"
                        id="grid-price" checked={input.highlight} onChange={handleChange} 
          value={input.is_diskon}
 name="is_diskon" type="checkbox" placeholder="0"  onClick={()=>setDiskon(!isDiskon)}/>
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mt-6 mx-2">
                        Status Diskon
                        </label>
                        </div>
                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                        {isDiskon && <>
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Harga Diskon
                        </label>
                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-discount" checked={input.highlight} onChange={handleChange} 
          value={input.harga_diskon}
 name="harga_diskon" type="text" placeholder="0"/>
                        </>
                        }
                    </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        
                        <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Jenis Barang
                        </label>
                        <div className="relative">
                            <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-type" checked={input.highlight} onChange={handleChange} 
          value={input.category}
 name="category">
                                <option>makanan</option>
                                <option>teknologi</option>
                                <option>minuman</option>
                                <option>kendaraan</option>
                                <option>hiburan</option>
                            </select>
                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                            </div>
                        </div>
                    </div>
                        <div className="w-full md:w-1/2 px-3">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Gambar Barang
                            </label>
                            <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-image" checked={input.highlight} onChange={handleChange} 
          value={input.image_url}
 name="image_url" type="text" placeholder="http://..."/>
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-3 mb-6">
                        <div className="w-full h-10 px-3 h-fit">
                            <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Deskripsi Barang
                            </label>
                            <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-10 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" id="grid-price" checked={input.highlight} onChange={handleChange} 
          value={input.description}
 name="description" type="text" placeholder="Deskripsi"/>
                        </div>
                    </div>

                    <div className="flex flex justify-center">
                        <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-2 px-4 rounded" checked={input.highlight} onChange={handleChange} 
 type="button" onClick={handleSubmit}>
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default UpdateFrom;