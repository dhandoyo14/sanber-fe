import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ProductProvider } from "./context/ProductContext";
import CreateProductPage from "./pages/CreateProductPage";
import ListProductPage from "./pages/ListProductPage";
import TableProductPage from "./pages/TableProductPage";
import UpdateProductPage from "./pages/UpdateProductPage";

function App() {
  return (
    <>
    <BrowserRouter>
      <ProductProvider>
        <Routes>
          <Route path="/" element={<ListProductPage />} />
          <Route path="/table" element={<TableProductPage />} />
          <Route path="/create" element={<CreateProductPage />} />
          <Route path="/update/:id" element={<UpdateProductPage />} />
        </Routes>
      </ProductProvider>
    </BrowserRouter>
    </>
  );
}

export default App;
