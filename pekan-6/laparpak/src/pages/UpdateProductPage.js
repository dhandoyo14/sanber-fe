import React from "react";
import Navbar from "../components/Navbar";
import UpdateForm from "../components/UpdateForm";

function UpdateProductPage() {
  return (
    <div>
      <Navbar />
      <UpdateForm />
    </div>
  );
}

export default UpdateProductPage;
