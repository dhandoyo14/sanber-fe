import React from "react";
import Form from "../components/Form";
import Navbar from "../components/Navbar";

function CreateProductPage() {
  return (
    <div>
      <Navbar />
      <Form />
    </div>
  );
}

export default CreateProductPage;
