import React, { useContext, useEffect } from "react";
import Navbar from "../components/Navbar";
import TableProduct from "../components/TableProduct";
import { ProductContext } from "../context/ProductContext";

function TableProductPage() {
  const { fetchProducts } = useContext(ProductContext);
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div>
      <Navbar />
      <TableProduct />
    </div>
  );
}

export default TableProductPage;
