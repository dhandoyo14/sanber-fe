import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ProductContext } from "../context/ProductContext";

function TableProduct() {
  const { products, fetchProducts, moveToCreate } = useContext(ProductContext);

  const onDelete = async (id) => {
    try {
      // fetch data
      const response = await axios.delete(
        `https://api-project.amandemy.co.id/api/products/${id}`
      );
      alert("Berhasil Menghapus Product");
      fetchProducts();
    } catch (error) {
      console.log(error);
      alert("Gagal Menghapus Product");
    }
  };
  return (
    <section className="">
      <h1 className="my-8 text-3xl font-bold text-center">Table Product</h1>
      <div className="max-w-4xl mx-auto w-full my-4">
        <button
          onClick={moveToCreate}
          className="px-6 py-2 bg-white text-blue-500 my-4 rounded-lg border-2 border-blue-500 float-right"
        >
          Create Product
        </button>
        <table className="border border-gray-500 w-full">
          <thead>
            <tr>
              <th className="border border-gray-500 p-2">ID</th>
              <th className="border border-gray-500 p-2">Name</th>
              <th className="border border-gray-500 p-2">Status Diskon</th>
              <th className="border border-gray-500 p-2">Harga</th>
              <th className="border border-gray-500 p-2">Harga Diskon</th>
              <th className="border border-gray-500 p-2">Image</th>
              <th className="border border-gray-500 p-2">Kategori</th>
              <th className="border border-gray-500 p-2">Action</th>
            </tr>
          </thead>
          <tbody>
            {products.map((item, index) => {
              return (
                <tr>
                  <td className="border border-gray-500 p-2">{item.id}</td>
                  <td className="border border-gray-500 p-2">{item.name}</td>
                  <td className="border border-gray-500 p-2">
                    {item.is_diskon === true ? "Mati" : "Aktif"}</td>
                    
                  <td className="border border-gray-500 p-2">{item.harga.toLocaleString('en-US', {
  style: 'currency',
  currency: 'IDR',
})}</td>
                  <td className="border border-gray-500 p-2">{item.harga_diskon.toLocaleString('en-US', {
  style: 'currency',
  currency: 'IDR',
})}</td>
                  <td className="border border-gray-500 p-2">
                    <img src={item.image_url} alt="" className="w-64" />
                  </td>
                  <td className="border border-gray-500 p-2">{item.category}
                  </td>

                  <td className="border border-gray-500 p-2">
                    <div className="flex gap-2">
                      <Link to={`/update/${item.id}`}>
                        <button className="px-3 py-1 bg-yellow-600 text-white">
                          Update
                        </button>
                      </Link>
                      <button
                        onClick={() => onDelete(item.id)}
                        className="px-3 py-1 bg-red-600 text-white"
                      >
                        Delete
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </section>
  );
}

export default TableProduct;
