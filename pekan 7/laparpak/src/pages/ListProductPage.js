import React, { useContext, useEffect } from "react";
import Product from "../components/Product";
import Navbar from "../components/Navbar";
import { ProductContext } from "../context/ProductContext";
import { Helmet } from "react-helmet";

function ListProductPage() {
  const { products, fetchProducts, loading, status } =
    useContext(ProductContext);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <section>
      <Helmet>
        <title>Home Page</title>
      </Helmet>
      <Navbar />
      <h1 className="my-8 text-3xl font-bold text-center">List Product</h1>
      {loading === false ? (
        <div className="mx-10 grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
          {products.map((item, index) => {
            return <Product data={item} />;
          })}
        </div>
      ) : (
        <h1 className="text-center my-6 text-3xl font-bold">Loading ....</h1>
      )}
    </section>
  );
}

export default ListProductPage;
