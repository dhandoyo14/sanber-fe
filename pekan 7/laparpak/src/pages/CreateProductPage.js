import React from "react";
import Form from "../components/Form";
import Navbar from "../components/Navbar";
import { Helmet } from "react-helmet";


function CreateProductPage() {
  return (
    <div>
      <Helmet>
        <title>Form Page</title>
      </Helmet>
      <Navbar />
      <Form />
    </div>
  );
}

export default CreateProductPage;
